from flask import Flask, jsonify
import pymongo
from pymongo import MongoClient

app = Flask (__name__) 


# class AnimalFarm():
def get_db():
    client = MongoClient(host=  'test_mongodb',
                                port=27017, 
                                username='root',
                                password='pass', 
                                authSource='admin')

    db = client["user_post_db"]
    return db

@app.route('/')
def ping_server():
    return "Test server"


@app.route('/users')
def get_stored_users():
    db = get_db()
    _users = db.users_doc.find()
    users = [{"id": user["id"], "name": user["name"], "username": user["username"]} for user in _users]
    return jsonify([{"status":"success"}, {"users":users}])


@app.route('/posts')
def get_stored_posts():
    db = get_db()
    _posts = db.posts_doc .find()
    posts = [{"id": post["id"], "title": post["title"], "body": post["body"]} for post in _posts]
    return jsonify([{"status":"sucess"},{"posts":posts}])


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
